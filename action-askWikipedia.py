#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from hermes_python.hermes import Hermes
from hermes_python.ontology import *
import wikipedia as wiki
import re


def subscribe_intent_callback(hermes, intent_message):
    conf = None
    action_wrapper(hermes, intent_message, conf)


def action_wrapper(hermes, intent_message, conf):
    """ Write the body of the function that will be executed once the intent is recognized. 
    In your scope, you have the following objects : 
    - intent_message : an object that represents the recognized intent
    - hermes : an object with methods to communicate with the MQTT bus following the hermes protocol. 
    - conf : a dictionary that holds the skills parameters you defined. 
      To access global parameters use conf['global']['parameterName']. 
      For end-user parameters use conf['secret']['parameterName'] 

    Refer to the documentation for further details. 
    """
    
    if(len(intent_message.slots.article_indicator) >= 1):
        wiki.set_lang("DE")
    
        # Get article content
        article = str(intent_message.slots.article_indicator.first().value)
        result = wiki.summary(article, sentences=5, auto_suggest=True)
    
        # Delete special characters
        result = result.split("==")[0]
        result = re.sub('[^A-Za-z0-9ÄÖÜäöüßẞ €$.,]+', '', result)
    
        # Try to get a description not to long and not to short
        splitted = result.split(".")
        for i in reversed(range(1, len(splitted))):
            result = ". ".join(splitted[0:i])
            if (len(result) < 250):
                break
    else:
        result = "Ich habe leider das Suchwort nicht verstanden."

    current_session_id = intent_message.session_id
    hermes.publish_end_session(current_session_id, result)


if __name__ == "__main__":
    with Hermes("localhost:1883") as h:
        h.subscribe_intent("wraith11:askWikipedia", subscribe_intent_callback) \
            .start()
